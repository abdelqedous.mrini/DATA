# Vérifie s'il y a des mises à jour disponibles
$UpdateSession = New-Object -ComObject Microsoft.Update.Session
$UpdateSearcher = $UpdateSession.CreateUpdateSearcher()
$SearchResult = $UpdateSearcher.Search("IsInstalled=0")
$Updates = $SearchResult.Updates

if ($Updates.Count -eq 0) {
    Write-Host "Aucune mise à jour disponible."
} else {
    Write-Host "Mises à jour disponibles. En cours de téléchargement et d'installation..."
    # Installe les mises à jour disponibles
    $Installer = New-Object -ComObject Microsoft.Update.Installer
    $Installer.Updates = $Updates
    $InstallationResult = $Installer.Install()
    if ($InstallationResult.ResultCode -eq 2) {
        Write-Host "Les mises à jour ont été installées avec succès."
    } else {
        Write-Host "Erreur lors de l'installation des mises à jour."
    }
}
